module PassphraseV2
(
  askPassphrase
  , getPassphrase
  , isValid
) where

import Data.Char
import Data.Maybe
import Control.Monad
import Control.Monad.Trans.Maybe
import Control.Monad.IO.Class

-- This typeclass contains 'lift' to go one level up the stack
-- import Control.Monad.Trans.Class

askPassphrase :: MaybeT IO ()
askPassphrase = do liftIO $ putStr "Enter your new passphrase (v2):"
                   value <- getPassphrase
                   liftIO $ putStrLn "Storing in database..."

getPassphrase :: MaybeT IO String
getPassphrase = do s <- liftIO getLine
                   guard (isValid s) -- MonadPlus provides guard.
                   return s

-- The validation test could be anything we want it to be.
isValid :: String -> Bool
isValid s = length s >= 8
            && any isAlpha s
            && any isNumber s
            && any isPunctuation s
