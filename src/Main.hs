module Main where

import qualified PassphraseV1 as V1
import qualified PassphraseV2 as V2
import Control.Monad.Trans.Maybe

main :: IO ()
main = do V1.askPassphrase
          runMaybeT V2.askPassphrase
          putStrLn "Done"
