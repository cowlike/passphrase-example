module PassphraseV1
(
  askPassphrase
  , getPassphrase
  , isValid
) where

import Data.Char
import Data.Maybe

askPassphrase :: IO ()
askPassphrase = do putStr "Enter your new passphrase (v1): "
                   pass <- getPassphrase
                   if isJust pass
                     then putStrLn "Storing in database..." -- do stuff
                     else putStrLn "Passphrase invalid."

getPassphrase :: IO (Maybe String)
getPassphrase = do
                  s <- getLine
                  if isValid s then return $ Just s
                               else return Nothing

-- The validation test could be anything we want it to be.
isValid :: String -> Bool
isValid s = length s >= 8
            && any isAlpha s
            && any isNumber s
            && any isPunctuation s
